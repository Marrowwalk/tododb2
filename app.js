const toDoInput = document.querySelector('.todo-input');
const toDoButton = document.querySelector('.todo-btn');
const toDoList = document.querySelector('.todo-list');
const filterOption = document.querySelector('.filter-todo');

toDoButton.addEventListener("click", addBtnEventHandler);
toDoList.addEventListener("click", deleteCheck);
filterOption.addEventListener("click", filterTodos);


function addBtnEventHandler(event) {
  event.preventDefault();
  console.log("Clicked");
  const createDivContainer = document.createElement("div");
  createDivContainer.classList.add("todo");

  const createLi = document.createElement("li");
  createLi.innerText = toDoInput.value;
  createLi.classList.add("todo-item");

  createDivContainer.appendChild(createLi);

  const completeBtn = document.createElement("button");
  completeBtn.innerHTML = '<i class="fas fa-check"></i>';
  completeBtn.classList.add("complete-btn");
  createDivContainer.appendChild(completeBtn);

  const removeBtn = document.createElement("button");
  removeBtn.innerHTML = '<i class="fas fa-trash"></i>';
  removeBtn.classList.add("remove-btn");
  createDivContainer.appendChild(removeBtn);

  toDoList.appendChild(createDivContainer);

  toDoInput.value = "";
}

function deleteCheck(e) {
  const item = e.target;
  if (item.classList[0] === "remove-btn") {
    const todo = item.parentElement;
    todo.classList.add("fall")
    todo.addEventListener("transitionend", function() {
        todo.remove();
    })
  }
  if (item.classList[0] ===  "complete-btn") {
      const todo = item.parentElement;
      todo.classList.toggle("complete")
  }
}

function filterTodos(e) {
    const todos = toDoList.childNodes;
    todos.forEach(function(todo) {
        switch(e.target.value) {
            case "all":
                todo.style.display = 'flex';
                break;
                case "completed":
                    if (todo.classList.contains("complete")) {
                        todo.style.display = 'flex';
                    } else {
                        todo.style.display = 'none';
                    }
                    break;
                    case "pending":
                        if  (!todo.classList.contains("complete")) {
                            todo.style.display  = 'flex';
                        } else {
                            todo.style.display  = 'none';
                        }
                        break;
        }
    })
}
